package sn.ucad.master2.ejb.session;

import java.util.List;

import javax.ejb.Remote;

import sn.ucad.master2.ejb.entity.Compte;
@Remote
public interface IbanqueRemote {
	public void addCompte(Compte c);
	public List<Compte> consulterComptes();
	public Compte consulterCompte(Long code);
	public void verser(Long code,double montant);
	public void retirer(Long code,double montant);
}
