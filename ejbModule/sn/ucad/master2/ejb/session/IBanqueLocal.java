package sn.ucad.master2.ejb.session;

import java.util.List;

import javax.ejb.Local;

import sn.ucad.master2.ejb.entity.Compte;
@Local
public interface IBanqueLocal {
	public void addCompte(Compte c);
	public List<Compte> consulterComptes();
	public Compte consulterCompte(Long code);
	public void verser(Long code,double montant);
	public void retirer(Long code,double montant);
}
