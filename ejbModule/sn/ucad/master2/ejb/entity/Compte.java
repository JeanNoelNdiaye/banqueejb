package sn.ucad.master2.ejb.entity;

import java.util.Date;

public class Compte {
        private Long code;
        private Double  solde;
        private Date dateCreation;
        
        public Compte(Long code, Double solde, Date dateCreation) {
			super();
			this.code = code;
			this.solde = solde;
			this.dateCreation = dateCreation;
		}
        
        
		public Long getCode() {
			return code;
		}
		public void setCode(Long code) {
			this.code = code;
		}
		public Double getSolde() {
			return solde;
		}
		public void setSolde(Double solde) {
			this.solde = solde;
		}
		public Date getDateCreation() {
			return dateCreation;
		}
		public void setDateCreation(Date dateCreation) {
			this.dateCreation = dateCreation;
		}
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((code == null) ? 0 : code.hashCode());
			result = prime * result + ((dateCreation == null) ? 0 : dateCreation.hashCode());
			result = prime * result + ((solde == null) ? 0 : solde.hashCode());
			return result;
		}
		
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Compte other = (Compte) obj;
			if (code == null) {
				if (other.code != null)
					return false;
			} else if (!code.equals(other.code))
				return false;
			if (dateCreation == null) {
				if (other.dateCreation != null)
					return false;
			} else if (!dateCreation.equals(other.dateCreation))
				return false;
			if (solde == null) {
				if (other.solde != null)
					return false;
			} else if (!solde.equals(other.solde))
				return false;
			return true;
		}
		
        
        
}
